//
//  VINModel.h
//  VINDemo
//
//  Created by Akhil on 1/13/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VINModel : NSObject
@property(nonatomic,strong) NSString *purchaseDate;
@property(nonatomic,strong) NSString *VIN;
@property(nonatomic,strong) NSString *dealerID;
@property(nonatomic,strong) NSString *todayDate;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic, strong) NSString *salesType;
@end
