//
//  OrderModel.h
//  VINDemo
//
//  Created by Niharika on 12/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderModel : NSObject

@property(nonatomic,strong) NSString *orderDate;
@property(nonatomic,strong) NSString *orderNo;
@property(nonatomic,strong) NSString *orderID;
@property(nonatomic,strong) NSString *dealerName;
@property(nonatomic,strong) NSString *orderType;
@property(nonatomic,strong) NSString *status;

@end
