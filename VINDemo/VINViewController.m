//
//  VINViewController.m
//  VINDemo
//
//  Created by Neha on 08/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#import "VINViewController.h"
#import "VINDetailViewController.h"
#import "NIDropDown.h"
#import "VINModel.h"
#import "BarCodeViewController.h"
#import "AppDelegate.h"
@interface VINViewController()<NIDropDownDelegate,ACScanBarDelegate,UIImagePickerControllerDelegate, UITextFieldDelegate, NSXMLParserDelegate>{
    NIDropDown *selectModel;
    NIDropDown *selectSeries;
    NIDropDown *selectSalesType;
    int count;
    bool flag;
}
@property NSString *soapMessage;
@property NSString *currentElement;
@property NSMutableData *webResponseData;
@property NSArray *allResponse;

@end

@implementation VINViewController
@synthesize soapMessage, webResponseData, currentElement;


-(void)viewDidLoad
{
    [super viewDidLoad];
    
    flag = false;
    self.modelArray = @[@"G0432F4S", @"G0432F5S", @"G0432F6S"];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor greenColor]} forState:UIControlStateHighlighted];
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self._selectModelView.layer setBorderWidth:1.0f];
    [self._selectModelView.layer setBorderColor:[UIColor blackColor].CGColor];
    
    [self._selectSeriesView.layer setBorderWidth:1.0f];
    [self._selectSeriesView.layer setBorderColor:[UIColor blackColor].CGColor];
    
    [self._selectSalesTypeView.layer setBorderWidth:1.0f];
    [self._selectSalesTypeView.layer setBorderColor:[UIColor blackColor].CGColor];
    
    [self._salesDateView.layer setBorderWidth:1.0f];
    [self._salesDateView.layer setBorderColor:[UIColor blackColor].CGColor];
    
//    [self.searchButton.layer setBorderWidth:1.0f];
//    [self.searchButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.searchButton.layer setCornerRadius:4.0f];
    
//    [self.cancelButton.layer setBorderWidth:1.0f];
//    [self.cancelButton.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.cancelButton.layer setCornerRadius:4.0f];
    
    [self.datePicker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (IBAction)selectModelAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    [self.VinTextField setAlpha:0.2];
    [self.VinTextField setUserInteractionEnabled:FALSE];
    
    count = 1;
    if(selectModel == nil) {
        
        CGFloat height = 30 * self.modelArray.count;
        selectModel = [[NIDropDown alloc] showDropDown:self._selectModelView
                                       heightOfView: height
                                    dataArrayForRow:self.modelArray];
        selectModel.delegate = self;
    }
    else
    {
        [selectModel hideDropDown:self._selectModelView];
        selectModel = nil;
    }
}

- (IBAction)selectSeriesAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    [self.VinTextField setAlpha:0.2];
    [self.VinTextField setUserInteractionEnabled:FALSE];
    count = 2;
    if(selectSeries == nil) {
        
        CGFloat height = 30 * self.modelArray.count;
        selectSeries = [[NIDropDown alloc] showDropDown:self._selectSeriesView
                                           heightOfView: height
                                        dataArrayForRow:self.modelArray];
        selectSeries.delegate = self;
    }
    else
    {
        [selectSeries hideDropDown:self._selectSeriesView];
        selectSeries = nil;
    }
}


- (IBAction)selectSalesTypeAction:(UIButton *)sender {
    
    [self.view endEditing:YES];
    [self.VinTextField setAlpha:0.2];
    [self.VinTextField setUserInteractionEnabled:FALSE];
    count = 3;
    if(selectSalesType == nil) {
        
        CGFloat height = 30 * self.modelArray.count;
        selectSalesType = [[NIDropDown alloc] showDropDown:self._selectSalesTypeView
                                           heightOfView: height
                                        dataArrayForRow:self.modelArray];
        selectSalesType.delegate = self;
    }
    else
    {
        [selectSalesType hideDropDown:self._selectSalesTypeView];
        selectSalesType = nil;
    }

}

- (IBAction)openleftMenu:(id)sender {
    [self presentLeftMenuViewController:nil];

}

- (IBAction)cancelButtonAction:(UIButton *)sender {
    self.VinTextField.text = @"";
    self._salesDateLabel.text = @"";
    self._selectModelLabel.text = @"";
    self._selectSalesTypeLabel.text = @"";
    self._selectSeriesLabel.text = @"";
    
//    self._selectModelLabel.textColor = [UIColor grayColor];
//    self._selectModelLabel.text = @"Model";
//    self._selectSeriesLabel.textColor = [UIColor grayColor];
//    self._selectSeriesLabel.text = @"Series";
//    self._salesDateLabel.textColor = [UIColor grayColor];
//    self._salesDateLabel.text= @"Sales Date";
//    self._selectSalesTypeLabel.textColor = [UIColor grayColor];
//    self._selectSalesTypeLabel.text = @"Sale Type";
    
    [self.VinTextField setAlpha:1.0];
    [self.VinTextField setUserInteractionEnabled:TRUE];
    
    [self._selectModelLabel setAlpha:1.0];
    [self._selectModelLabel setUserInteractionEnabled:TRUE];
    
    [self._selectSeriesLabel setAlpha:1.0];
    [self._selectSeriesLabel setUserInteractionEnabled:TRUE];
    
    [self._salesDateLabel setAlpha:1.0];
    [self._salesDateLabel setUserInteractionEnabled:TRUE];
    
    [self._selectSalesTypeLabel setAlpha:1.0];
    [self._selectSalesTypeLabel setUserInteractionEnabled:TRUE];
    
}

- (IBAction)nextScreen:(UIButton *)sender {
    
    if([self.VinTextField hasText]){
        self.responseArray = [NSArray array];
        //first create the soap envelope
        soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                       "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                       "<soap:Body>"
                       "<GetwarrantyDetail xmlns=\"http://tempuri.org/\">"
                       "<VinNumber>%@</VinNumber>"
                       "</GetwarrantyDetail>"
                       "</soap:Body>"
                       "</soap:Envelope>", self.VinTextField.text];
        
        
        //Now create a request to the URL
        NSURL *url = [NSURL URLWithString:@"http://115.112.147.14/KMADCS/webservice.asmx"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        //ad required headers to the request
        [theRequest addValue:@"115.112.147.14" forHTTPHeaderField:@"Host"];
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/GetwarrantyDetail" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        //initiate the request
        NSURLConnection *connection =
        [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if(connection)
        {
            webResponseData = [NSMutableData data] ;
        }
        else
        {
            NSLog(@"Connection is NULL");
        }
    }else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter the vin number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

}

- (IBAction)scanButton:(UIButton *)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }else{
        BarCodeViewController *barobjVC=[[BarCodeViewController alloc]init];
        barobjVC.view.frame=self.view.frame;
        barobjVC.delegate=self;
        [self presentViewController:barobjVC animated:YES completion:nil];

        /*
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
         */
        
    }
}

- (IBAction)chooseDate:(UIButton *)sender {
    [self.VinTextField setAlpha:0.2];
    [self.VinTextField setUserInteractionEnabled:FALSE];
    if(flag){
        [self.datePicker setHidden:TRUE];
        [self.searchButton setHidden:FALSE];
        [self.cancelButton setHidden:FALSE];
    }else{
        [self.datePicker setHidden:FALSE];
        [self.searchButton setHidden:TRUE];
        [self.cancelButton setHidden:TRUE];
    }
    flag = !flag;
}

- (void)datePickerChanged:(UIDatePicker *)datePicker
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *strDate = [dateFormatter stringFromDate:datePicker.date];
    self._salesDateLabel.text = strDate;
    self._salesDateLabel.textColor = [UIColor blackColor];
}

#pragma marks - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma marks - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if(textField == self.VinTextField)
    {
    
    if(![self.VinTextField hasText]){
        [self._selectModelLabel setAlpha:1.0];
        [self._selectModelLabel setUserInteractionEnabled:TRUE];
        
      [self._selectSeriesLabel setAlpha:1.0];
        [self._selectSeriesLabel setUserInteractionEnabled:TRUE];
        
      [self._salesDateLabel setAlpha:1.0];
        [self._salesDateLabel setUserInteractionEnabled:TRUE];
        
       [self._selectSalesTypeLabel setAlpha:1.0];
        [self._selectSalesTypeLabel setUserInteractionEnabled:TRUE];
        
        [self._selectModelLabel becomeFirstResponder];
    }
    else
    {
        [self.VinTextField resignFirstResponder];
    }
    
    }
    
    else if (textField == self._selectModelLabel)
    {
        [self._selectSeriesLabel becomeFirstResponder];
    }
    
    else if (textField == self._selectSeriesLabel)
    {
        [self._selectSalesTypeLabel becomeFirstResponder];
    }
    
    else if (textField == self._selectSalesTypeLabel)
    {
        [self._salesDateLabel becomeFirstResponder];
    }
    
    else if (textField == self._salesDateLabel)
    {
        [self._salesDateLabel resignFirstResponder];
    }
    return TRUE;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == self.VinTextField)
    {
        
   [self._selectModelLabel setAlpha:0.3];
        [self._selectModelLabel setUserInteractionEnabled:FALSE];
    
    [self._selectSeriesLabel setAlpha:0.3];
        [self._selectSeriesLabel setUserInteractionEnabled:FALSE];
    
    [self._salesDateLabel  setAlpha:0.3];
        [self._salesDateLabel setUserInteractionEnabled:FALSE];
    
  [self._selectSalesTypeLabel setAlpha:0.3];
        [self._selectSalesTypeLabel setUserInteractionEnabled:FALSE];
        
    }
    
}

#pragma marks - NIDropDownDelegateMethods

- (void) niDropDown:(NIDropDown *)sender
{
    selectModel = nil;
    selectSeries = nil;
    selectSalesType = nil;
}

- (void) niDropDown:(NIDropDown *)sender didSelectRowAtIndex:(NSInteger)index
{
    if(count==1){
        self._selectModelLabel.text = self.modelArray[index];
        self._selectModelLabel.textColor = [UIColor blackColor];
    }else if(count == 2){
        self._selectSeriesLabel.text = self.modelArray[index];
        self._selectSeriesLabel.textColor = [UIColor blackColor];
    }else if(count == 3){
        self._selectSalesTypeLabel.text = self.modelArray[index];
        self._selectSalesTypeLabel.textColor = [UIColor blackColor];
    }
}

#pragma marks - NIDropDownDelegateMethods
//Implement the connection delegate methods.
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.webResponseData  setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.webResponseData  appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Some error in your Connection. Please try again.");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"Received %lu Bytes", (unsigned long)[webResponseData length]);
    NSString *theXML = [[NSString alloc] initWithBytes:
                        [webResponseData mutableBytes] length:[webResponseData length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",theXML);
    
    //now parsing the xml
    
    NSData *myData = [theXML dataUsingEncoding:NSUTF8StringEncoding];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:myData];
    
    //setting delegate of XML parser to self
    xmlParser.delegate = self;
    
    // Run the parser
    @try{
        BOOL parsingResult = [xmlParser parse];
        if(parsingResult)
        {
            
            NSLog(@"parsing result = %@",self.responseArray);
            if([self.responseArray count] > 0){
                VINModel *vinModel=[[VINModel alloc]init];
                for (int i=0 ; i<[self.responseArray count]; i++) {
                    vinModel.VIN=[[self.responseArray objectAtIndex:i] objectAtIndex:0];
                    NSArray *dateArray = [[[self.responseArray objectAtIndex:i] objectAtIndex:1] componentsSeparatedByString:@" "];
    //                vinModel.purchaseDate= [[self.responseArray objectAtIndex:i] objectAtIndex:1];
                    vinModel.purchaseDate = [dateArray firstObject];
                    vinModel.dealerID=[[self.responseArray objectAtIndex:i] objectAtIndex:2];
                    vinModel.desc=[[self.responseArray objectAtIndex:i] objectAtIndex:3];;
                    
                    vinModel.todayDate = [[self.responseArray objectAtIndex:i] objectAtIndex:5];;
                    vinModel.salesType=@"R-Retail";
                    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
                    [dateformate setDateFormat:@"MM-dd-yyyy"]; // Date formater
                    NSString *date = [dateformate stringFromDate:[NSDate date]]; // Convert date to string
                    //vinModel.todayDate = date;
                }
                
                VINDetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"VINDetailViewController"];
                detailController.vinModel = vinModel;
                [self.navigationController pushViewController:detailController animated:YES];
            }else{
                if(self.errorResponse)
                {
                    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Server Error" message:self.errorResponse delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                else{
                    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No records found related to VIN number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                    self.VinTextField.text = @"";
                }
            }

        }
    }
    @catch (NSException* exception)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Server Error" message:[exception reason] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
}

//Implement the NSXmlParserDelegate methods
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:
(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    currentElement = elementName;
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([currentElement isEqualToString:@"GetwarrantyDetailResult"]) {
        
        
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        self.responseArray  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"self.allResponse %@",self.responseArray);
    }
    
    
    if ([currentElement isEqualToString:@"faultstring"]) {
        
        self.errorResponse  = string;
        
        NSLog(@"self.allResponse %@",self.errorResponse);
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSLog(@"Parsed Element : %@", currentElement);
}


#pragma mark ACScanner Delegate
- (void) scannedBarCodeNumber:(NSString *)barcode{
    self.VinTextField.text=barcode;
    NSLog(@"barcode is %@",barcode);
}

@end
