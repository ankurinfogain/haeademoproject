//
//  AppDelegate.h
//  VINDemo
//
//  Created by Neha on 08/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray *ordersArray;

@end

