//
//  VINMapViewController.m
//  VINDemo
//
//  Created by Niharika on 13/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "VINMapViewController.h"

@interface VINMapViewController ()
{

CLLocationManager *locationManager;

    BOOL drawn;
}

@end

@implementation VINMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    
    [self.view addSubview:self.mapView];
    self.mapView.delegate = self;
    
    // Ensure that you can view your own location in the map view.
    [self.mapView setShowsUserLocation:YES];
    //
    //    //Instantiate a location object.
    locationManager = [[CLLocationManager alloc] init];
    
    //Make this controller the delegate for the location manager.
    [locationManager setDelegate:self];
    
    //Set some parameters for the location object.
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
    [self drawAnnotations];
    //    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)drawAnnotations
{
    CLLocationCoordinate2D coord1;
    // Set the lat and long.
    coord1.latitude =33.700509;//33.700509
    coord1.longitude = -117.943321;//-
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coord1, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    // Add an annotation
    if(!drawn)
    {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//        CLLocationCoordinate2D coord1;
//        // Set the lat and long.
//        coord1.latitude =33.700509;//33.700509
//        coord1.longitude = -117.943321;//-117.943321

        point.coordinate = coord1;
        point.title = @"Dealer AK006";
        //    point.subtitle = @"I'm here!!!";
        
        MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D coord2;
        // Set the lat and long.
        coord2.latitude =33.697976;//33.700509
        coord2.longitude = -117.944348;//-117.943321
        point1.coordinate =coord2;
        point1.title = @"PDC 1";
        // point1.subtitle = @"";
        
        MKPointAnnotation *point3 = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D coord3;
        // Set the lat and long.
        coord3.latitude = 33.699172;//33.699172
        coord3.longitude = -117.941515;//-117.941515
        point3.coordinate =coord3;
        point3.title = @"PDC 2";
        
        
        MKPointAnnotation *point4 = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D coord4;
        // Set the lat and long.
        coord4.latitude = 33.698690;//33.698690
        coord4.longitude = -117.943103;//-117.943103
        point4.coordinate =coord4;
        point4.title = @"PDC 3";
        
        //33.697976 //-117.944348
        [self.mapView addAnnotation:point];
        [self.mapView addAnnotation:point1];
        [self.mapView addAnnotation:point3];
        [self.mapView addAnnotation:point4];
        
        
        drawn=YES;
    }

}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* newLocation = [locations lastObject];
    
    
    
}

#pragma mark- Mapview delegate


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
//    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
//    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
//    
//    // Add an annotation
//    if(!drawn)
//    {
//    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
//        CLLocationCoordinate2D coord1;
//        // Set the lat and long.
//        coord1.latitude =33.700509;//33.700509
//        coord1.longitude = -117.943321;//-117.943321
//        point.coordinate = coord1;
//        point.title = @"Dealer ABC";
////    point.subtitle = @"I'm here!!!";
//    
//    MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
//    CLLocationCoordinate2D coord2;
//    // Set the lat and long.
//    coord2.latitude =33.697976;//33.700509
//    coord2.longitude = -117.944348;//-117.943321
//    point1.coordinate =coord2;
//    point1.title = @"PDC 1";
//   // point1.subtitle = @"";
//    
//    MKPointAnnotation *point3 = [[MKPointAnnotation alloc] init];
//    CLLocationCoordinate2D coord3;
//    // Set the lat and long.
//    coord3.latitude = 33.699172;//33.699172
//    coord3.longitude = -117.941515;//-117.941515
//    point3.coordinate =coord3;
//    point3.title = @"PDC 2";
//    
//        
//    MKPointAnnotation *point4 = [[MKPointAnnotation alloc] init];
//    CLLocationCoordinate2D coord4;
//    // Set the lat and long.
//    coord4.latitude = 33.698690;//33.698690
//    coord4.longitude = -117.943103;//-117.943103
//    point4.coordinate =coord3;
//    point4.title = @"Supplier C";
//        
//        //33.697976 //-117.944348
//    [self.mapView addAnnotation:point];
//    [self.mapView addAnnotation:point1];
//    [self.mapView addAnnotation:point3];
//        [self.mapView addAnnotation:point4];
//
//        
//        drawn=YES;
//    }
    
}



- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
    NSLog(@"Annotation Title :%@",[annotation title]);
    if([[annotation title] isEqualToString:@"Dealer AK006"])
    {
    annView.pinColor=MKPinAnnotationColorGreen;
    }
    else{
    annView.pinColor=MKPinAnnotationColorRed;
    }
    annView.canShowCallout = YES;
    return annView;
}
 

@end
