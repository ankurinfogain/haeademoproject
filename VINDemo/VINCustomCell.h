//
//  VINCustomCell.h
//  VINDemo
//
//  Created by Akhil on 1/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VINCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *desc;

@end
