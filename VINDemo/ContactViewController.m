//
//  ContactViewController.m
//  VINDemo
//
//  Created by Niharika on 13/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "ContactViewController.h"
#import "VINCustomCell.h"


@interface ContactViewController ()

@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here
    
    self.dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Sudeep Kaul", @"Name",@"dealerabc@gmail.com",@"Email" ,@"8580011112" ,@"Contact No",@"AK006",@"Dealer Code",@"10550 Talbert Avenue, Fountain Valley, CA 92708",@"Address" ,nil];
    
    self.array = [[NSArray alloc] initWithObjects:@"Name",@"Email",@"Contact No",@"Dealer Code",@"Address", nil];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"VINCustomCell";
    
    VINCustomCell *cell = (VINCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VINCustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.labelTitle.text = [self.array objectAtIndex:indexPath.row];
    cell.desc.text = [self.dictionary objectForKey:[self.array objectAtIndex:indexPath.row]];
    
    return cell;
    
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
    //                             cellIdentifier];
    //
    //    if (cell == nil) {
    //        cell = [[UITableViewCell alloc]initWithStyle:
    //                UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    //    }
    //
    ////    NSString *string = [self.dictionary objectForKey:@"Dealer"];
    //
    //    [cell.textLabel setText:[self.array objectAtIndex:indexPath.row]];
    //    [cell.detailTextLabel setText:[self.dictionary objectForKey:[self.array objectAtIndex:indexPath.row]]];
    //    return cell;
    
}


- (IBAction)openLeftMenu:(id)sender {
    [self presentLeftMenuViewController:nil];
}
@end
