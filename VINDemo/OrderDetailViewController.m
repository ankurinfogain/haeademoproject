//
//  OrderDetailViewController.m
//  VINDemo
//
//  Created by Niharika on 12/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "OrderModel.h"



@interface OrderDetailViewController ()
{
    NSArray *orders;
    NSArray *searchResults;
    BOOL isSearch;
    
}

@property NSString *soapMessage;
@property NSString *currentElement;
@property NSMutableData *webResponseData;
@property NSArray *allResponse;
@property NSString *errorResponse;

@end

@implementation OrderDetailViewController

@synthesize soapMessage, webResponseData, currentElement;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
   
    orders  = [NSArray array];
    
    [self.activity setHidden:YES];
    [self.searchTxtField.layer setBorderWidth:1.0f];
    [self.searchTxtField.layer setBorderColor:[UIColor blackColor].CGColor];

    
//    [self.searchBtn.layer setBorderWidth:1.0f];
//    [self.searchBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [self.searchBtn.layer setCornerRadius:4.0f];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark- table view delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//{ if(isSearch)
//    return searchResults.count;
//    else
//        
    return orders.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    orderCell = [tableView dequeueReusableCellWithIdentifier:@"orderDetail"];
    OrderModel *order;
//    if(isSearch)
//       order=[searchResults objectAtIndex:indexPath.row];
//    else
       order=[orders objectAtIndex:indexPath.row];
    
    orderCell.dealerName.text= @"AK006";//order.dealerName;
    orderCell.orderNoLbl.text=order.orderNo;
    orderCell.orderDate.text=order.orderDate;
    if([order.orderType isEqualToString:@"E"]){
    orderCell.orderTypeLbl.text= @"Emergency";
    }
    else{
        orderCell.orderTypeLbl.text= @"Stock";
    }
    orderCell.statusLbl.text=order.status;
    
    if([order.status isEqualToString:@"Submitted"])
    {
    [orderCell.editBtn setHidden:YES];
    }
    else{
      [orderCell.editBtn setHidden:NO];
    }
    //adding shadow
    
    orderCell.outerView.layer.cornerRadius = 5;
    orderCell.outerView.layer.masksToBounds = YES;
    orderCell.editBtn.tag = indexPath.row;
    [orderCell.outerView.layer setBorderWidth:1.0f];
    [orderCell.outerView.layer setBorderColor:[UIColor colorWithRed:181.0f/255.0f green:0 blue:33.0/255.0f alpha:1.0f].CGColor];

    return orderCell;

}


#pragma mark -search bar delgates
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"partNo contains[c] %@", self.searchBar.text];
    if([searchBar.text isEqualToString:@""])
    {
        isSearch=NO;
    }
    else
    {
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"partNo contains[c] %@", self.searchBar.text];
        searchResults = [orders filteredArrayUsingPredicate:resultPredicate];
        isSearch=YES;
        
    }
    [self.orderTableView reloadData];
    
}
//
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.orderTableView reloadData];
    
}

// called when text ends editing
//- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
//{
//    [self.searchBar resignFirstResponder];
//    [self.view endEditing:YES];
//    if([searchBar.text isEqualToString:@""])
//    {
//        isSearch=NO;
//    }
//    else
//    {
//        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"topic contains[c] %@", self.searchBar.text];
//        searchResults = [_trainings filteredArrayUsingPredicate:resultPredicate];
//        isSearch=YES;
//    }
//    [self.tableView reloadData];
//    
//}


- (IBAction)validatePartNo:(id)sender {
    
    if([[self.searchTxtField text] length] >0){
        self.responseOrderArray = [NSArray array];
        orders = [NSArray array];
        [_partNolbl setText:@""];
        [self.activity setHidden:NO];
        [self.activity startAnimating];
        
        //first create the soap envelope
        soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                       "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                       "<soap:Body>"
                       "<CheckPartValidation xmlns=\"http://tempuri.org/\">"
                       "<PartNumber>%@</PartNumber>"
                       "</CheckPartValidation>"
                       "</soap:Body>"
                       "</soap:Envelope>", [self.searchTxtField text]];
        
        //Now create a request to the URL
        NSURL *url = [NSURL URLWithString:@"http://115.112.147.14/KMADCS/webservice.asmx"];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
        
        //ad required headers to the request
        [theRequest addValue:@"115.112.147.14" forHTTPHeaderField:@"Host"];
        [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest addValue: @"http://tempuri.org/CheckPartValidation" forHTTPHeaderField:@"SOAPAction"];
        [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
        
        //initiate the request
        NSURLConnection *connection =
        [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
        
        if(connection)
        {
            webResponseData = [NSMutableData data] ;
        }
        else
        {
            NSLog(@"Connection is NULL");
            [self.activity setHidden:YES];
            [self.activity stopAnimating];
        }
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter part number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}




- (IBAction)searchOrder:(id)sender {
        if([[self.searchTxtField text] length] >0){
    self.responseOrderArray = [NSArray array];
            [self.activity setHidden:NO];
            [self.activity startAnimating];
    orders = [NSArray array];
    
    //first create the soap envelope
    soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                   "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                   "<soap:Body>"
                   "<GetOrderDetail xmlns=\"http://tempuri.org/\">"
                   "<PartNumber>%@</PartNumber>"
                   "</GetOrderDetail>"
                   "</soap:Body>"
                   "</soap:Envelope>", [self.searchTxtField text]];
    
    //Now create a request to the URL
    NSURL *url = [NSURL URLWithString:@"http://115.112.147.14/KMADCS/webservice.asmx"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    //ad required headers to the request
    [theRequest addValue:@"115.112.147.14" forHTTPHeaderField:@"Host"];
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/GetOrderDetail" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    //initiate the request
    NSURLConnection *connection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if(connection)
    {
        webResponseData = [NSMutableData data] ;
    }
    else
    {
        NSLog(@"Connection is NULL");
        [self.activity setHidden:YES];
        [self.activity stopAnimating];
       
    }
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please enter part number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
}

- (IBAction)editOrder:(id)sender {
    
    orderVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddOrderVC"];
    orderVC.orderModel = [orders objectAtIndex:[sender tag]];
    orderVC.partNumber = self.searchTxtField.text;
    [self.navigationController pushViewController:orderVC animated:YES];
    
}

- (IBAction)openLeftMenu:(id)sender {
    [self presentLeftMenuViewController:nil];
}

//Implement the connection delegate methods.
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.webResponseData  setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.webResponseData  appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Some error in your Connection. Please try again.");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.activity setHidden:YES];
    [self.activity stopAnimating];
    NSLog(@"Received %lu Bytes", (unsigned long)[webResponseData length]);
    NSString *theXML = [[NSString alloc] initWithBytes:
                        [webResponseData mutableBytes] length:[webResponseData length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"XML %@",theXML);
    //now parsing the xml
    
    NSData *myData = [theXML dataUsingEncoding:NSUTF8StringEncoding];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:myData];
    
    //setting delegate of XML parser to self
    xmlParser.delegate = self;
    
    // Run the parser
    @try{
        BOOL parsingResult = [xmlParser parse];
        if(parsingResult)
        {
         
       NSLog(@"parsing result = %@",self.responseOrderArray);
            
            if([self.validParts count]>0){
                
                [_partNolbl setText:[[self.validParts objectAtIndex:0] objectAtIndex:2]];
                [self searchOrder:nil];
            }else
            {

            if([self.responseOrderArray count] > 0 ){
                for (int i=0 ; i<[self.responseOrderArray count]; i++) {
                    OrderModel *order1=[[OrderModel alloc]init];
                    order1.orderID = [[self.responseOrderArray objectAtIndex:i] objectAtIndex:0];
                    order1.dealerName=[[self.responseOrderArray objectAtIndex:i] objectAtIndex:1];;
                    order1.orderNo= [[self.responseOrderArray objectAtIndex:i] objectAtIndex:2];
                    
                    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
                    order1.orderDate= dateString;
                    NSLog(@"%@",dateString);
                    
                    
                    
                    order1.orderType=[[self.responseOrderArray objectAtIndex:i] objectAtIndex:4];;
                    order1.status=[[self.responseOrderArray objectAtIndex:i] objectAtIndex:5];
                    
                    orders = [orders arrayByAddingObject:order1];
                }
                [self.orderTableView reloadData];
            }
            else{
                if(self.errorResponse)
                {
                    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Server Error" message:self.errorResponse delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                else{
                    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No orders found related to part number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                    
                    orders = [NSArray array];
                    [self.orderTableView reloadData];
                    
                }
            }
            
            }
        }
    }
    @catch (NSException* exception)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Server Error" message:[exception reason] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
}


-(void)oo{
    
}

//Implement the NSXmlParserDelegate methods
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:
(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    currentElement = elementName;
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([currentElement isEqualToString:@"GetOrderDetailResult"]) {
       
        self.validParts = [NSArray array];
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        self.responseOrderArray  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"self.allResponse %@",self.responseOrderArray);
    }
    
    
    if ([currentElement isEqualToString:@"CheckPartValidationResult"]) {
        
        
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        self.validParts  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"self.allResponse %@",self.validParts);
    }
    
    if ([currentElement isEqualToString:@"faultstring"]) {
        
        self.errorResponse  = string;
        
        NSLog(@"self.allResponse %@",self.errorResponse);
    }
 
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSLog(@"Parsed Element : %@", currentElement);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.searchTxtField resignFirstResponder];
    
    return YES;
}

- (IBAction)scanBarcode:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        BarCodeViewController *barobjVC=[[BarCodeViewController alloc]init];
        barobjVC.view.frame=self.view.frame;
        barobjVC.delegate=self;
        [self presentViewController:barobjVC animated:YES completion:nil];
        
        /*
         UIImagePickerController *picker = [[UIImagePickerController alloc] init];
         picker.sourceType = UIImagePickerControllerSourceTypeCamera;
         picker.delegate = self;
         [self presentViewController:picker animated:YES completion:nil]; */
    }
    else
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissModalViewControllerAnimated:YES];
}

#pragma mark ACScanner Delegate
- (void) scannedBarCodeNumber:(NSString *)barcode{
    self.searchTxtField.text=barcode;
    NSLog(@"barcode is %@",barcode);
}



@end
