//
//  OrderDetailTableViewCell.h
//  VINDemo
//
//  Created by Niharika on 12/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *dealerName;
@property (weak, nonatomic) IBOutlet UILabel *orderDate;
@property (weak, nonatomic) IBOutlet UILabel *statusLbl;

@property (weak, nonatomic) IBOutlet UILabel *orderTypeLbl;
@property (weak, nonatomic) IBOutlet UIView *outerView;
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@end
