//
//  VINViewController.h
//  VINDemo
//
//  Created by Neha on 08/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface VINViewController : UIViewController

@property (strong, atomic) NSArray *modelArray;

@property (weak, nonatomic) IBOutlet UITextField *VinTextField;

@property (weak, nonatomic) IBOutlet UIView *_selectModelView;

@property (weak, nonatomic) IBOutlet UITextField *_selectModelLabel;

@property (weak, nonatomic) IBOutlet UIView *_selectSeriesView;

@property (weak, nonatomic) IBOutlet UITextField *_selectSeriesLabel;

@property (weak, nonatomic) IBOutlet UIView *_selectSalesTypeView;

@property (weak, nonatomic) IBOutlet UITextField *_selectSalesTypeLabel;

@property (weak, nonatomic) IBOutlet UIView *_salesDateView;

@property (weak, nonatomic) IBOutlet UITextField *_salesDateLabel;

@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

- (IBAction)selectModelAction:(UIButton *)sender;

- (IBAction)selectSeriesAction:(UIButton *)sender;

- (IBAction)selectSalesTypeAction:(UIButton *)sender;

- (IBAction)nextScreen:(UIButton *)sender;

- (IBAction)scanButton:(UIButton *)sender;

- (IBAction)chooseDate:(UIButton *)sender;

@property (strong, atomic) NSArray *responseArray;

@property (strong, nonatomic) NSString *errorResponse;

- (IBAction)openleftMenu:(id)sender;
- (IBAction)cancelButtonAction:(UIButton *)sender;

@end
