//
//  ACBarcodeScanner.h
//  BarCodeScannerDemo
//
//  Created by Ankur on 14/01/16.
//
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ACBCamera) {
    ACBCameraBack,
    ACBCameraFront
};

typedef NS_ENUM(NSUInteger, ACBTorchMode) {
    ACBTorchModeOff,
    ACBTorchModeOn,
    ACBTorchModeAuto
};

@interface ACBarcodeScanner : NSObject

@property (nonatomic, assign) ACBCamera camera;


@property (nonatomic, assign) ACBTorchMode torchMode;


@property (nonatomic, assign) CGRect scanRect;

@property (nonatomic, strong) CALayer *previewLayer;
@property (nonatomic, copy) void (^didStartScanningBlock)();


@property (nonatomic, copy) void (^resultBlock)(NSArray *codes);

- (instancetype)initWithPreviewView:(UIView *)previewView;


- (instancetype)initWithMetadataObjectTypes:(NSArray *)metaDataObjectTypes
                                previewView:(UIView *)previewView;

/**
 *  Returns whether the camera exists in this device.
 *
 *  @return YES if the device has a camera.
 */
+ (BOOL)cameraIsPresent;

+ (BOOL)scanningIsProhibited;

+ (void)requestCameraPermissionWithSuccess:(void (^)(BOOL success))successBlock;


- (void)startScanning;


- (void)startScanningWithResultBlock:(void (^)(NSArray *codes))resultBlock;
- (void)stopScanning;
- (BOOL)isScanning;
- (BOOL)hasTorch;
- (void)toggleTorch;

@end
