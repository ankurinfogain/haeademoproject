//
//  BarCodeViewController.h
//  BarCodeScannerDemo
//
//  Created by Ankur on 14/01/16.
//  Copyright © 2016 Ankur. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ACScanBarDelegate <NSObject>

@optional
-(void)optionalForFutureUse;

@required
- (void) scannedBarCodeNumber:(NSString *)barcode;
@end


@interface BarCodeViewController : UIViewController
@property (nonatomic, assign) id<ACScanBarDelegate> delegate;
@end
