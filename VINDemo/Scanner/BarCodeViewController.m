//
//  BarCodeViewController.m
//  BarCodeScannerDemo
//
//  Created by Ankur on 14/01/16.
//  Copyright © 2016 Ankur. All rights reserved.
//

#import "BarCodeViewController.h"
#import "ACBarcodeScanner.h"
@interface BarCodeViewController ()
@property (nonatomic, weak) IBOutlet UIView *previewView;
@property (nonatomic, weak) IBOutlet UIButton *toggleScanningButton;
@property (nonatomic, weak) IBOutlet UIButton *toggleTorchButton;
@property (nonatomic, strong) ACBarcodeScanner *scanner;


@end

@implementation BarCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self toggleScanningTapped:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Scanner

- (ACBarcodeScanner *)scanner {
    if (!_scanner) {
        _scanner = [[ACBarcodeScanner alloc] initWithPreviewView:_previewView];
    }
    return _scanner;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - Scanning

- (void)startScanning {
    
    [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
        for (AVMetadataMachineReadableCodeObject *code in codes) {
            if (code.stringValue) {
                [self.delegate scannedBarCodeNumber:code.stringValue];
               // [self.delegate optionalForFutureUse];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }];
    
    [self.toggleScanningButton setTitle:@"Stop Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor redColor];
}

- (void)stopScanning {
    [self.scanner stopScanning];
    [self.toggleScanningButton setTitle:@"Start Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = self.view.tintColor;
    [self dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark - Helper Methods

- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([ACBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![ACBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Scanning Unavailable"
                                message:message
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil] show];
   
}


- (IBAction)toggleScanningTapped:(id)sender {
    if ([self.scanner isScanning]) {
        [self stopScanning];
        self.toggleTorchButton.selected = YES;
    } else {
        [ACBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
    }
}




- (IBAction)toggleTorchTapped:(id)sender {
    if (self.scanner.torchMode == ACBTorchModeOff || self.scanner.torchMode == ACBTorchModeAuto) {
        self.scanner.torchMode = ACBTorchModeOn;
        self.toggleTorchButton.selected=NO;
    } else {
        self.scanner.torchMode = ACBTorchModeOff;
        self.toggleTorchButton.selected=YES;
    }
}



- (void)viewWillDisappear:(BOOL)animated {
    [self.scanner stopScanning];
    [super viewWillDisappear:animated];
}

@end
