//
//  ContactViewController.h
//  VINDemo
//
//  Created by Niharika on 13/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface ContactViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, atomic) NSDictionary *dictionary;
@property (strong, atomic) NSArray *array;
- (IBAction)openLeftMenu:(id)sender;

@end
