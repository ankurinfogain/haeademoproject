//
//  VINDetailViewController.m
//  VINDemo
//
//  Created by Akhil on 1/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "VINDetailViewController.h"
#import "VINCustomCell.h"

@interface VINDetailViewController ()

@end

@implementation VINDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here
    
    NSLog(@"%@", self.vinModel);
    
    self.dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"CA310 / GARDEN GROVE HYUNDAI", @"Model Number" ,@"KMHEC4A43FA123456" ,@"VIN" ,@"2015 SONATA HYBRID LIMITED ( G0432F4S )" ,@"Description" ,@"05/23/2015" ,@"Purchase Date" ,@"Sales" ,@"Sales Type" ,@"01/08/2016" ,@"Today's Date", nil];
    
    self.array = [[NSArray alloc] initWithObjects:@"Model Number",@"VIN",@"Series", @"Sales Date", @"Sales Type", @"Warranty Date", nil];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
 
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"VINCustomCell";
    
    VINCustomCell *cell = (VINCustomCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"VINCustomCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.labelTitle.text = [self.array objectAtIndex:indexPath.row];
    if(indexPath.row == 0)
        cell.desc.text = self.vinModel.dealerID;
    else if (indexPath.row == 1)
        cell.desc.text = self.vinModel.VIN;
    else if (indexPath.row == 2)
        cell.desc.text = self.vinModel.desc;
    else if (indexPath.row == 3)
        cell.desc.text = self.vinModel.purchaseDate;
    else if (indexPath.row == 4)
        cell.desc.text = self.vinModel.salesType;
    else if (indexPath.row == 5)
    {
        NSString * dateStr = [[self.vinModel.todayDate componentsSeparatedByString:@" "] objectAtIndex:0];
        cell.desc.text = dateStr;
    }
    
    return cell;
    
}

@end
