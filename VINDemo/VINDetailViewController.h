//
//  VINDetailViewController.h
//  VINDemo
//
//  Created by Akhil on 1/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VINModel.h"

@interface VINDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, atomic) NSDictionary *dictionary;
@property (strong, atomic) NSArray *array;

@property (nonatomic, retain) VINModel *vinModel;

@end
