//
//  OrderViewController.m
//  VINDemo
//
//  Created by Neha on 12/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#define KEmptyString @""

#import "OrderViewController.h"
#import "OrderDetailViewController.h"
#import "AppDelegate.h"
#import "OrderModel.h"
#import "NIDropDown.h"

@interface OrderViewController ()<NIDropDownDelegate,ACScanBarDelegate>
{
    OrderDetailViewController* _orderDetailviewController;
    NIDropDown *selectOrderType;
}

@property NSString *soapMessage;
@property NSString *currentElement;
@property NSMutableData *webResponseData;
@property NSArray *allResponse;

@end

@implementation OrderViewController

@synthesize soapMessage, webResponseData, currentElement, orderModel, partNumber;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
     self.orderTypeModelArray = @[@"Stock", @"Emergency"];
    [self addBorderToUIElements];
    
    if(self.orderModel.orderNo)
    {
        self.orderNumTxtF.text = self.orderModel.orderNo;
    }
    
    if(self.partNumber)
    {
        self.partNumTxtF.text = self.partNumber;
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addBorderToUIElements
{
    [self.orderTypeView.layer setBorderWidth:1.0f];
    [self.orderTypeView.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.orderTypeLabel.layer setBorderWidth:1.0f];
    [self.orderTypeLabel.layer setBorderColor:[UIColor blackColor].CGColor];
    
    [self.partNumTxtF.layer setBorderWidth:1.0f];
    [self.partNumTxtF.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.quantityTxtF.layer setBorderWidth:1.0f];
    [self.quantityTxtF.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.vinTxtF.layer setBorderWidth:1.0f];
    [self.vinTxtF.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.orderNumTxtF.layer setBorderWidth:1.0f];
    [self.orderNumTxtF.layer setBorderColor:[UIColor blackColor].CGColor];
    
   // [self.saveBtn.layer setBorderWidth:1.0f];
    //[self.saveBtn.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.saveBtn.layer setCornerRadius:4.0f];
    
//    [self.cancelBtn.layer setBorderWidth:1.0f];
//    [self.cancelBtn.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.cancelBtn.layer setCornerRadius:4.0f];
    
//    [self.submitBtn.layer setBorderWidth:1.0f];
//    [self.submitBtn.layer setBorderColor:[UIColor blackColor].CGColor];
    [self.submitBtn.layer setCornerRadius:4.0f];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)openCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        BarCodeViewController *barobjVC=[[BarCodeViewController alloc]init];
        barobjVC.view.frame=self.view.frame;
        barobjVC.delegate=self;
        [self presentViewController:barobjVC animated:YES completion:nil];

        /*
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        [self presentViewController:picker animated:YES completion:nil]; */
    }
    else
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [picker dismissModalViewControllerAnimated:YES];
}

//- (IBAction)saveOrder
//{
//    OrderModel* orderModal = [[OrderModel alloc]init];
//    orderModal.VIN = [self.vinTxtF text];
//    orderModal.partNo = [self.partNumTxtF text];
//    orderModal.orderNo = [self.orderNumTxtF text];
//    orderModal.quantity = [[self.quantityTxtF text] integerValue];
//    orderModal.orderType = @"Regular";
//    
//    _orderDetailviewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailVC"];
//    AppDelegate* appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//    [appDelegate.ordersArray addObject:orderModal];
//    [self.navigationController pushViewController:_orderDetailviewController animated:YES];
//}

- (IBAction)saveOrSubmitOrder:(UIButton *)sender
{
    //first create the soap envelope
    soapMessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?> <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"> <soap:Body> <CreatePartOrder xmlns=\"http://tempuri.org/\"> <OrdreIdx>%d</OrdreIdx> <DealerName>%@</DealerName><OrderNumber>%@</OrderNumber><OrderType>%@</OrderType><OrderStatus>%d</OrderStatus><PartNumber>%@</PartNumber><quantity>%d</quantity><VinNumber>%@</VinNumber></CreatePartOrder></soap:Body></soap:Envelope>", self.orderModel.orderID.intValue, @"AK006", self.orderNumTxtF.text, self.orderTypeLabel.text,(int)sender.tag, self.partNumTxtF.text, self.quantityTxtF.text.intValue, self.vinTxtF.text];
    
    NSLog(@"%@", soapMessage);
    
    //Now create a request to the URL
    NSURL *url = [NSURL URLWithString:@"http://115.112.147.14/KMADCS/webservice.asmx"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMessage length]];
    
    //ad required headers to the request
    [theRequest addValue:@"115.112.147.14" forHTTPHeaderField:@"Host"];
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/CreatePartOrder" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapMessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    //initiate the request
    NSURLConnection *connection =
    [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if(connection)
    {
        webResponseData = [NSMutableData data] ;
    }
    else
    {
        NSLog(@"Connection is NULL");
    }

}

- (IBAction)cancelOrder
{
    [self.partNumTxtF setText:KEmptyString];
    [self.orderNumTxtF setText:KEmptyString];
    [self.quantityTxtF setText:KEmptyString];
    [self.vinTxtF setText:KEmptyString];
    [self.orderTypeLabel setText:@"Order Type"];
    self.orderTypeLabel.textColor = [UIColor lightGrayColor];
}

- (IBAction)selectOrderType:(UIButton *)sender {
    
    [self.view endEditing:YES];
    
    if(selectOrderType == nil) {
        
        CGFloat height = 30 * self.orderTypeModelArray.count;
        selectOrderType = [[NIDropDown alloc] showDropDown:self.orderTypeView
                                              heightOfView: height
                                           dataArrayForRow:self.orderTypeModelArray];
        selectOrderType.delegate = self;
    }
    else
    {
        [selectOrderType hideDropDown:self.orderTypeView];
        selectOrderType = nil;
    }
    
}

- (IBAction)viewOrders:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - NIDropDownDelegate

- (void) niDropDown:(NIDropDown *)sender
{
    selectOrderType = nil;
}

- (void) niDropDown:(NIDropDown *)sender didSelectRowAtIndex:(NSInteger)index
{
        self.orderTypeLabel.text = self.orderTypeModelArray[index];
        self.orderTypeLabel.textColor = [UIColor grayColor];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.partNumTxtF)
    {
        [self.orderNumTxtF becomeFirstResponder];
    }
    else if (textField == self.orderNumTxtF)
    {
        [self.vinTxtF becomeFirstResponder];
    }
    else if (textField == self.vinTxtF)
    {
        [self.quantityTxtF becomeFirstResponder];
    }
    else if (textField == self.quantityTxtF)
    {
        [self.quantityTxtF resignFirstResponder];
    }

    return YES;
}

//Implement the connection delegate methods.
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.webResponseData  setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.webResponseData  appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"Some error in your Connection. Please try again.");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"Received %lu Bytes", (unsigned long)[webResponseData length]);
    NSString *theXML = [[NSString alloc] initWithBytes:
                        [webResponseData mutableBytes] length:[webResponseData length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",theXML);
    
    //now parsing the xml
    
    NSData *myData = [theXML dataUsingEncoding:NSUTF8StringEncoding];
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:myData];
    
    //setting delegate of XML parser to self
    xmlParser.delegate = self;
    
    // Run the parser
    @try{
        BOOL parsingResult = [xmlParser parse];
        if(parsingResult)
        {
            if([[[self.response objectAtIndex:0] objectAtIndex:0] isEqualToString:@"S"]){
                UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Order created succcessfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                self.partNumTxtF.text=@"";
                self.vinTxtF.text=@"";
                self.quantityTxtF.text=@"";
                self.orderNumTxtF.text=@"";
                
                
                //[self.navigationController popViewControllerAnimated:YES];
            }
            

        }
    }
    @catch (NSException* exception)
    {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Server Error" message:[exception reason] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        return;
    }
}

//Implement the NSXmlParserDelegate methods
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:
(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    currentElement = elementName;
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([currentElement isEqualToString:@"CreatePartOrderResult"]) {
        NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
        self.response  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        
        NSLog(@"self.allResponse %@",[self.response objectAtIndex:0]);
        
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSLog(@"Parsed Element : %@", currentElement);
}

#pragma mark ACScanner Delegate
- (void) scannedBarCodeNumber:(NSString *)barcode{
    _partNumTxtF.text=barcode;
    NSLog(@"barcode is %@",barcode);
}


@end
