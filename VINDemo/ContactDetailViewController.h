//
//  ContactDetailViewController.h
//  VINDemo
//
//  Created by Niharika on 13/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ContactDetailViewController : UIViewController<MKMapViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate,MKAnnotation>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;;


@end
