//
//  ViewController.m
//  VINDemo
//
//  Created by Neha on 08/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#define KUserNameKey @"userName"

#import "ViewController.h"
#import "OrderViewController.h"
#import "DEMORootViewController.h"

@interface ViewController ()
{
    UITextField* activeField;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     NSString *savedUserName = [[NSUserDefaults standardUserDefaults] stringForKey:KUserNameKey];
    [self.userNameTxt setText:savedUserName];
    
    if(![savedUserName isEqualToString:@""])
    {
        [self.saveButton setSelected:YES];
    }
    
    //[self.loginView.layer setBorderWidth:1.0f];
//    /[self.loginView.layer setBorderColor:[UIColor blackColor].CGColor];
    
    [self.saveButton setBackgroundImage:[UIImage imageNamed:@"empty_radio_button.png"] forState:UIControlStateNormal];
    [self.saveButton setBackgroundImage:[UIImage imageNamed:@"selected_radio.png"] forState:UIControlStateSelected];
    
    [self.enableButton setBackgroundImage:[UIImage imageNamed:@"empty_radio_button.png"] forState:UIControlStateNormal];
    [self.enableButton setBackgroundImage:[UIImage imageNamed:@"selected_radio.png"] forState:UIControlStateSelected];
    
    [self.loginBtn.layer setCornerRadius:4.0f];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginBtn:(id)sender
{
    
    //For User Name and Password Validation If Required;
    NSString *strRegExp=@"[A-Za-z0-9._]+";
    NSPredicate *userNameTest=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",strRegExp];
    
    NSString *errorMessage = nil;
    NSInteger *errorCode = nil;
    
    if (self.userNameTxt.text.length == 0)
    {
        errorMessage = @"Please Enter User Name";
        errorCode = (NSInteger *)1;
    }
    else if ([userNameTest evaluateWithObject:self.userNameTxt.text]==NO)
    {
        errorMessage = @"Please Enter Valid User Name";
        errorCode = (NSInteger *)1;
        
    }
    else if(self.passwordTxt.text.length == 0)
    {
        errorMessage = @"Please Enter Password";
        errorCode = (NSInteger *)2;
        
    }
    else if(![self.userNameTxt.text isEqualToString:@"test"] && ![self.userNameTxt.text isEqualToString:@"AK006"])
    {
        errorMessage = @"Invalid Username";
        errorCode = (NSInteger *)3;

    }
    else if(![self.passwordTxt.text isEqualToString:@"test"])
    {
        errorMessage = @"Invalid Password";
        errorCode = (NSInteger *)3;
    }
    else
    {
       // OrderViewController *myNewVC = [[OrderViewController alloc] init];
        if([self.saveButton isSelected])
        {
            [[NSUserDefaults standardUserDefaults] setObject:self.userNameTxt.text forKey:KUserNameKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
//    UITabBarController *tbc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabBar"];
//        tbc.selectedIndex=0;
//        [self presentViewController:tbc animated:YES completion:nil];
//        
        
        DEMORootViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"rootController"];
     //   [self.navigationController pushViewController: vc animated:YES];
        [self presentViewController:vc animated:YES completion:nil];

        
    }
    
    if(errorMessage)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}

#pragma marks - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    activeField.clearButtonMode = UITextFieldViewModeAlways;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userNameTxt)
    {
        [self.passwordTxt becomeFirstResponder];
    }
    else if (textField == self.passwordTxt)
    {
        [self.passwordTxt resignFirstResponder];
    }
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger length = textField.text.length + string.length - range.length;
    
    UITextField *otherTextField = self.userNameTxt;
    
    if (textField == self.userNameTxt)
    {
        otherTextField = self.passwordTxt;
    }
    
    if (length == 0 || ([otherTextField hasText] == NO))
    {
        [self.loginBtn setEnabled:false];
    }
    else
    {
        [self.loginBtn setEnabled:true];
    }
    
    if ([textField isEqual:self.userNameTxt])
    {
        return !([textField.text length]>50 && [string length] > range.length);
    }
    else
    {
        return !([textField.text length]>20 && [string length] > range.length);
    }
}


- (IBAction)radioButton:(UIButton *)sender {
    switch ([sender tag]) {
        case 0:
            if([self.saveButton isSelected]==YES)
            {
                [self.saveButton setSelected:NO];
                [self.enableButton setSelected:NO];
            }
            else{
                [self.saveButton setSelected:YES];
                [self.enableButton setSelected:NO];
            }
            
            break;
        case 1:
            if([self.enableButton isSelected]==YES)
            {
                [self.enableButton setSelected:NO];
                [self.saveButton setSelected:NO];
            }
            else{
                [self.enableButton setSelected:YES];
                [self.saveButton setSelected:NO];
            }
            
            break;
        default:
            break;
    }
}
@end
