//
//  NIDropDown.m
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import "NIDropDown.h"
#import "QuartzCore/QuartzCore.h"

@interface NIDropDown ()
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIView *popTarget;
@property(nonatomic, retain) NSArray *list;
@end

@implementation NIDropDown

-(id)showDropDown:(UIView *) view heightOfView:(CGFloat) height dataArrayForRow:(NSArray *)dataArray{
    self.popTarget = view;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect viewFrame = view.frame;
        self.list = [NSArray arrayWithArray:dataArray];
        self.frame = CGRectMake(viewFrame.origin.x, viewFrame.origin.y + viewFrame.size.height, viewFrame.size.width, 0);
        self.layer.shadowOffset = CGSizeMake(-5, 5);
        
        self.layer.masksToBounds = NO;
        self.layer.shadowOpacity = 0.5;
        
        self.table = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, 0, viewFrame.size.width, 0)];
        self.table.delegate = self;
        self.table.dataSource = self;
//        self.table.layer.cornerRadius = 4.0f;
        self.table.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
        self.table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.table.separatorColor = [UIColor blackColor];
        self.table.scrollEnabled = NO;
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.2];
        self.frame = CGRectMake(viewFrame.origin.x, viewFrame.origin.y + viewFrame.size.height, viewFrame.size.width, height);
        self.table.frame = CGRectMake(0, 0, viewFrame.size.width, height);
        [UIView commitAnimations];
        [view.superview addSubview:self];
        [self addSubview:self.table];
    }
    return self;
}

-(void)hideDropDown:(UIView *)view {
    CGRect viewFrame = view.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    self.frame = CGRectMake(viewFrame.origin.x, viewFrame.origin.y + viewFrame.size.height, viewFrame.size.width, 0);
    self.table.frame = CGRectMake(0, 0, viewFrame.size.width, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont fontWithName:@"System-regular" size:17.0f];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    cell.textLabel.text = [self.list objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor blackColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideDropDown:self.popTarget];
    [self.delegate niDropDown:self didSelectRowAtIndex:indexPath.row];
    [self.delegate niDropDown:self];
    
}

@end
