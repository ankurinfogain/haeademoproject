//
//  OrderDetailViewController.h
//  VINDemo
//
//  Created by Niharika on 12/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDetailTableViewCell.h"
#import "OrderViewController.h"
#import "RESideMenu.h"
#import "BarCodeViewController.h"


@interface OrderDetailViewController : UIViewController<UITableViewDelegate,UIImagePickerControllerDelegate, UITableViewDataSource,NSXMLParserDelegate, UITextFieldDelegate, ACScanBarDelegate>
{
    OrderDetailTableViewCell *orderCell;
    OrderViewController *orderVC;
    
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *orderTableView;
@property (strong, nonatomic) IBOutlet NSArray *responseOrderArray;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UILabel *partNolbl;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;

- (IBAction)searchOrder:(id)sender;
- (IBAction)validatePartNo:(id)sender;
- (IBAction)editOrder:(id)sender;
- (IBAction)scanBarcode:(id)sender;

- (IBAction)openLeftMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property (weak, nonatomic) NSArray *validParts;
@end
