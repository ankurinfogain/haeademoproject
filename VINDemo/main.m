//
//  main.m
//  VINDemo
//
//  Created by Neha on 08/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
