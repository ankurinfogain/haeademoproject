//
//  ACStrings.h
//
//  Created by Ankur on 02/11/15.
//
//

#ifndef ACStrings_h
#define ACStrings_h


#pragma mark - User Defaults Keys

// left menu names

#define kLeftIcon1Name                              @"HOME"
#define kLeftIcon2Name                              @"SETTINGS"
#define kLeftIcon3Name                              @"VIN SEARCH"
#define kLeftIcon4Name                              @"DELETE ORDER"
#define kLeftIcon5Name                              @"QUICK SURVEY"
#define kLeftIcon6Name                              @"HELP"
#define kLeftIcon7Name                              @"CONTACT US"
#define kLeftIcon8Name                              @"LOGOUT"
#define kLeftIcon9Name                              @"OPTION1"
#define kLeftIcon10Name                             @""


// left menu images

#define kLeftIcon1                              @"lefticon1"
#define kLeftIcon2                              @"lefticon2"
#define kLeftIcon3                              @"lefticon3"
#define kLeftIcon4                              @"lefticon4"
#define kLeftIcon5                              @"lefticon5"
#define kLeftIcon6                              @"lefticon6"
#define kLeftIcon7                              @"lefticon7"
#define kLeftIcon8                              @"lefticon8"
#define kLeftIcon9                              @"lefticon8"
#define kLeftIcon10                             @"lefticon8"


#endif /* ACStrings_h */
