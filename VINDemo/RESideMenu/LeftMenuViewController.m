//
//  LeftMenuViewController.m
//  apartmentconnect
//
//  Created by Ankur Chauhan on 10/31/15.
//
//

#import "LeftMenuViewController.h"
#import "ACStrings.h"
#import "ListingTableViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "ContactViewController.h"
#define kReusableIdentifier @"cell"
#define kFontName           @"HelveticaNeue-light"


@interface LeftMenuViewController ()

@property (strong, nonatomic) NSArray * options;
@property (strong, nonatomic) NSArray * optionsImages;

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _avatorImageView.clipsToBounds = YES;

    // border radius
    [_avatorImageView.layer setCornerRadius:40.0f];
    // border
    [_avatorImageView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_avatorImageView.layer setBorderWidth:1.5f];
    
   // drop shadow
   // [_avatorImageView.layer setShadowColor:[UIColor blackColor].CGColor];
  // [_avatorImageView.layer setShadowOpacity:0.8];
  //  [_avatorImageView.layer setShadowRadius:3.0];
  //  [_avatorImageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    // Do any additional setup after loading the view.
    self.options = @[
                     kLeftIcon1Name,
                     kLeftIcon2Name,
                     kLeftIcon3Name,
                     kLeftIcon4Name,
                     kLeftIcon5Name,
                     kLeftIcon6Name,
                     kLeftIcon7Name,
                     kLeftIcon8Name,
                   //  kLeftIcon9Name,
                   //  kLeftIcon10Name,
                     ];
    
    self.optionsImages = @[
                           kLeftIcon1,
                           kLeftIcon2,
                           kLeftIcon3,
                           kLeftIcon4,
                           kLeftIcon5,
                           kLeftIcon6,
                           kLeftIcon7,
                           kLeftIcon8,
                          // kLeftIcon9,
                          // kLeftIcon10,
                           ];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.options count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ListingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ListCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    if (indexPath.row==1||indexPath.row==2||indexPath.row==3||indexPath.row==4||indexPath.row==5) {
//        [self adjustCellIconOffset:cell];
//   
//    }

        cell.optionLabel.text = [self.options objectAtIndex:indexPath.row];
        cell.iconImageView.image = [UIImage imageNamed:[self.optionsImages objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if ([[UIScreen mainScreen] bounds].size.width > 320)
        {
            cell.textLabel.font = [UIFont fontWithName:kFontName size:16.0f];
        }
        else
        {
            cell.textLabel.font = [UIFont fontWithName:kFontName size:14.0f];
        }
    return cell;
}


-(void)adjustCellIconOffset :(ListingTableViewCell *) cell{
    cell.iconImageView.frame=CGRectMake(35, cell.iconImageView.frame.origin.y, cell.iconImageView.frame.size.width, cell.iconImageView.frame.size.height);

    cell.optionLabel.frame=CGRectMake(75, cell.optionLabel.frame.origin.y, cell.optionLabel.frame.size.width, cell.optionLabel.frame.size.height);

}

#pragma mark - UITableViewDelegate

//- (void)setupControllerForSceneType:(igScene)sceneType
//{
//    IGRootVC *root = [kAppDelegate rootVC];
//    if (root.currentScene != sceneType)
//    {
//        NSString *storyboardIdentifier = [igStoryboardIdentifier storyboardIdentifierForScene:sceneType];
//        UIViewController *contentVC = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
//        [root setContentViewController:contentVC animated:YES];
//        [root hideMenuViewController];
//        root.currentScene = sceneType;
//    }
//    else
//    {
//        [root hideMenuViewController];
//    }
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
  //  NSLog(@"self.navigationController.viewControllers %@",self.navigationController.viewControllers);

    
    if (indexPath.row==0) {
        // home
        [self.sideMenuViewController hideMenuViewController];
        //    [self presentLeftMenuViewController:nil];
        //    [self presentRightMenuViewController:nil];

    }
    else if (indexPath.row==1){
        [self.sideMenuViewController hideMenuViewController];

    }
    else if (indexPath.row==2){
        [self.sideMenuViewController hideMenuViewController];
   
    }
    else if (indexPath.row==3){
        [self.sideMenuViewController hideMenuViewController];

    }
    else if (indexPath.row==4){
        [self.sideMenuViewController hideMenuViewController];

    }
    else if (indexPath.row==5){
        [self.sideMenuViewController hideMenuViewController];
 
    }
    else if (indexPath.row==6){
        [self.sideMenuViewController hideMenuViewController];
    }
    else if (indexPath.row==7){
        // logout
        [self dismissViewControllerAnimated:YES completion:nil];
       // NSArray *arrayVc=self.navigationController.viewControllers;
       // [self .navigationController popToViewController:[arrayVc objectAtIndex:0] animated:YES];
  
    }
    else if (indexPath.row==8){

    }
    else if (indexPath.row==9){
        // Logout

    }
    else{
    
    }
    
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 190;
    }
    return 44;
}
 */

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView * headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tblView.frame.size.width, 30)];
//    return headerView;
//}

//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 30;
//}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    static CGFloat previousOffsetY = 0;
//    CGFloat deltaOffsetY = scrollView.contentOffset.y - previousOffsetY;
//    previousOffsetY = scrollView.contentOffset.y;
//    
//    CGPoint center = imageViewLogo.center;
//    center.y -= deltaOffsetY;
//    imageViewLogo.center = center;
//}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)updateUserDetailsBtnClkd:(UIButton *)sender {
}

@end
