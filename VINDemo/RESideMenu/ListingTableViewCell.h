//
//  ListingTableViewCell.h
//  apartmentconnect
//
//  Created by Ankur on 02/11/15.
//
//

#import <UIKit/UIKit.h>

@interface ListingTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;

@end
