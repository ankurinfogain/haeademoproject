//
//  LeftMenuViewController.h
//  apartmentconnect
//
//  Created by Ankur Chauhan on 10/31/15.
//
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface LeftMenuViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *avatorImageView;
- (IBAction)updateUserDetailsBtnClkd:(UIButton *)sender;

@end
