//
//  OrderViewController.h
//  VINDemo
//
//  Created by Neha on 12/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"
#import "BarCodeViewController.h"

@interface OrderViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, NSXMLParserDelegate>

@property (weak, nonatomic) IBOutlet UITextField *partNumTxtF;
@property (weak, nonatomic) IBOutlet UITextField *orderNumTxtF;
@property (weak, nonatomic) IBOutlet UITextField *vinTxtF;
@property (weak, nonatomic) IBOutlet UITextField *quantityTxtF;
@property (strong, atomic) NSArray* orderTypeModelArray;
@property (weak, nonatomic) IBOutlet UIView *orderTypeView;
@property (weak, nonatomic) IBOutlet UILabel *orderTypeLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) NSArray *response;

@property (strong, atomic) OrderModel* orderModel;
@property (strong, atomic) NSString* partNumber;

- (IBAction)openCamera;
//- (IBAction)saveOrder;
- (IBAction)saveOrSubmitOrder:(UIButton *)sender;
- (IBAction)cancelOrder;
- (IBAction)selectOrderType:(UIButton *)sender;
- (IBAction)viewOrders:(id)sender;

@end
