//
//  VINCustomCell.m
//  VINDemo
//
//  Created by Akhil on 1/11/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "VINCustomCell.h"

@implementation VINCustomCell
@synthesize labelTitle, desc;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
