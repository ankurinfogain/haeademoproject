//
//  ContactDetailViewController.m
//  VINDemo
//
//  Created by Niharika on 13/01/16.
//  Copyright © 2016 Infogain. All rights reserved.
//

#import "ContactDetailViewController.h"

@interface ContactDetailViewController ()
{
    CLLocationManager *locationManager;
  

}
@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    
    [self.view addSubview:self.mapView];
    self.mapView.delegate = self;
    
    // Ensure that you can view your own location in the map view.
    [self.mapView setShowsUserLocation:YES];
//
//    //Instantiate a location object.
    locationManager = [[CLLocationManager alloc] init];
    
    //Make this controller the delegate for the location manager.
    [locationManager setDelegate:self];
    
    //Set some parameters for the location object.
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        [locationManager requestWhenInUseAuthorization];
    
    [locationManager startUpdatingLocation];
//    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
  
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* newLocation = [locations lastObject];
    
   
    
}

#pragma mark- Mapview delegate


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
    MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D coord;
    // Set the lat and long.
    coord.latitude = 28.6037739;
    coord.longitude = 77.3661788;
    point1.coordinate =coord;
    point1.title = @"2 point?";
    point1.subtitle = @"I'm here!!!";
    
    [self.mapView addAnnotation:point];
    [self.mapView addAnnotation:point1];

}

@end
