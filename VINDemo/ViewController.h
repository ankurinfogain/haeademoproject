//
//  ViewController.h
//  VINDemo
//
//  Created by Neha on 08/01/16.
//  Copyright (c) 2016 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *userNameTxt;

@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;

- (IBAction)loginBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;

@property (weak, nonatomic) IBOutlet UIView *loginView;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (weak, nonatomic) IBOutlet UIButton *enableButton;

- (IBAction)radioButton:(UIButton *)sender;
@end

